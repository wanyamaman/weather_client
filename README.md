# Weather Client

A Sinatra based weather client using the Open Weather Map API. Completed as part of a [challenge](https://gitlab.com/wanyamaman/weather_client/blob/master/CHALLENGE.md).
The app allows users to view random current weather information and search for the current weather in a particular city.

## RoadMap

Read the [roadmap](https://gitlab.com/wanyamaman/weather_client/blob/master/ROADMAP.md) to learn more about the trade-offs and challenges faced in designing this application as well as possible future improvements.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

This application uses:

- Ruby 2.3.3
- `Sinatra` gem
- `HTTParty` gem
- `Open Weather Map` API - [key](https://openweathermap.org/appid)

In order to run the application you will need `ruby` installed in your environment.
I recommend using [rbenv](https://github.com/rbenv/rbenv) to manage your ruby environment.

To set up the application environment run the following steps:

```sh
git clone git@gitlab.com:wanyamaman/weather_client.git

cd weather-client/

gem install sinatra -v 2.0.0

gem install httparty -v 0.15.15

```
Don't forget to add your api `key` to your environment. If you are using rbenv you can manage variables easily using [rbenv-vars](https://github.com/rbenv/rbenv-vars) by using the following:
```sh
# put this in .rbenv-vars or .bash_profile or anywhere that exposes values to your environment
open_weather_key=secret
```

### Installing

Once the repo has been cloned and all prerequisites met, run the following commands in the project root folder to get up and running.

```sh
# install dependencies
bundle install

# run the server
ruby app/app.rb

# or if you have foreman installed
foreman start
```
`capybary-webkit` requires `qt` to run. Follow instructions [here](https://github.com/thoughtbot/capybara-webkit/wiki/Installing-Qt-and-compiling-capybara-webkit) if `bundle install` fails with a `qtmake` error.

The application will be available at [http://localhost:4567](http://localhost:4567). For `foreman` the application is served at [http://localhost:5000](http://localhost:5000).

This launches the home page which loads random weather data and allows you to search for current weather by city and country.

Have Fun ;)

## Running the tests

This app uses [RSpec](http://rspec.info/) and [Capybara](http://teamcapybara.github.io/capybara/) for testing and the `vcr` gem for caching external api requests during tests.
Not all external requests are cached. However, to benefit from `vcr` caching you will need to run the test suite once.
The first run will make requests to an external api and cache them for faster follow up runs.

To run tests, type the following in the root folder of the project:
```ruby
bundle exec rspec
```

### Test folder structure

Tests are located in `/spec` folder at the root of the project and are structured as follows:

```sh
/app # sinatra server files
/features # Acceptance tests using Capybara. (uncached API calls)
/helpers  # Tests for application wide helpers
/models   # Data representation tests using RSpec. (no api calls)
/services # Open Weather Map api service tests. (cachable external api calls)
/support  # RSpec supporting modules
```
- After the first run `vcr` will create a folder called cassettes with cached api calls.
- You can run individual tests by appending its path to: `bundle exec rspec <path>`


## Built With

* [Open Weather Map](https://openweathermap.org/) - A publicly available weather api
* [Sinatra](http://www.sinatrarb.com/) - A ruby based web framework
* [Bootstrap V4-alpha.6](https://v4-alpha.getbootstrap.com/) - A HTML, CSS, and JS framework


## Authors

* **William Wanyama**


## License

This project is licensed under the [MIT](https://en.wikipedia.org/wiki/MIT_License) License.
