# Roadmap

Several factors affected the creation of this application. Some of them are discussed below:

## Design

Given the [requirements](https://gitlab.com/wanyamaman/weather_client/blob/master/CHALLENGE.md) of the application, it was decided that the best user experience would load the weather data asynchronously onto the page rather than navigating the user to a different page. It was also decided that the application did not need a database as all data could be fetched from the open weather map api.

### Frameworks
Three frameworks were considered, namely:
- `Rails` - Rails is quick to build with and easy to set up. It is also the framework I am most comfortable with. However, a basic rails setup comes with a lot of features which might be too heavy for such a basic app.
- `Vue JS` (or other `js` frameworks) - Vue is a light yet powerful front-end framework. Since the app does not require a database, a front-end framework on its own could suffice. Javascript is also excellent for loading data asynchronously. However, I would need to reacquaint myself with the framework as well as the javascript ecosystem.
- `Sinatra` - A light yet powerful ruby based framework. Sinatra is also quick to build with and easy to use. It's biggest drawback was my unfamiliarity with the framework having never used it before.

All three frameworks are equally as capable in completing this task. In the end I decided to use Sinatra as it uses ruby (my strongest language) and has access to a great selection of gems while having a light footprint. Aditionally I constantly seek to challenge myself by learning new things. In this case Sinatra met all criteria.

### Approach
I used an inside-out TDD strategy.

### User experience
- If a random location has a name, it will be displayed along with its weather information to the user. If it does not have a name _e.g a location in the middle of the ocean_, the information will be displayed with the title `Random Location`.

- The Open Weather Map api returns a best match when searching for cities alone or cities and countries. This behaviour was blocked in the application. If the api response has a different country or city to that that was queried by the user, a 404 error is returned by the application and the user is appropriately notified.


-------------------------------------------------------------------------------------------------------------------

## Challenges

- **Time** - The biggest challenge. This was a very busy period for me at work which meant I had to sporadically squeeze in time for development whenever the opportunity presented itself.
- **Sinatra** - I had to take time out to learn the particulars of Sinatra as I was unfamiliar with the framework.
- **Testing External Requests** - Testing Javascript from a ruby perspective has it's own challenges. `Capybara` is a powerful testing gem however I ran into issues in mocking internal requests that launch external requests when using `webmock` and `vcr`. This resulted in request caching being disabled for Capybara based tests (features).

--------------------------------------------------------------------------------------------------------------------

## Way forward

### Caching strategy
I did not get time to implement caching but given the opportunity I would consider two options:
1. Use Sinatra's in built caching techniques. I am not familiar with these yet.
2. Alternatively implement my own caching strategy.

**The characteristics for caching are as follows**:
- Caching should ideally be done on both the client and server side. Client side caching helps reduce requests from a single user. Server caching reduces api calls to the Open Weather Map API allowing the application to serve multiple users from one external API query.
- Only successful api fetches should be cached.
- Cache lookup should be fast.
- Weather information is not critical.
- Subsequent requests for the same city, city/country combination should be fetched from the cache if still `fresh`.
- A `freshness` window of 20 mins is acceptable.
- Caching random weather information is more complicated and would need to be estimated.

**My implementation for server side caching would be as follows**:
- Create a single instance of the cache object on application start-up.
- The object would store data in a private hash.
- Hash keys would be made of a hash consisting of the query e.g: `{city: 'paris', country: 'FR'}`.
- Hash values would be made of a hash containing the json response (in an array) and a timestamp e.g: `{created: <Time object>, data: [<json response>]}`.
- Therfore the cache object hash would have the following format: `{ {city: 'paris', country: 'FR'}: {created: <Time object>, data: [<json response>]}`
- Several random locations can be fetched and cached as a set in the array. This set would be given a single time stamp.
- The cache object would expose query methods for fetching data.
- It would return a random index from the data array if there is a cache hit, else it returns false.
- This would return random data from the random set or the same data for the specific case (a set of one).
- If data is found, a check is performed to compare the timestamp and the current time for freshness.
- If the data is stale, it would be removed from the cache hash and the cache object would return false, indicating a miss.
- Depending on the average server load, a separate process could be created that regularly clears the cache of stale values.

### Private/secured api endpoints
Currently, the app exposes a public api under `/api/weather/random` and `/api/weather/location`. There is no need for a public facing api as weather data can easily be accessed from the Open Weather Map website. These endpoints could be removed.
However, if they are to be maintained, the would need to be rate limited, version controlled, protected (require authentication), and made REST compliant.

### Exception handling
Currently, there is very minimal exception handling in the application. This would become critical if the application is to be deployed to production. Exceptions can catch errors from gems such as HTTParty as well as Timeout events, while allowing for better logging. Exceptions would allow the application to gracefully recover from such errors and provide appropriate messages to the user.

### Better error messages
User error feedback can also be improved. More checks can be made on the response status code which can then in turn customize the error message passed to the user.

### Application structure
Even though the application follows a modular structure it can still be expanded further in order for the application scale well. Areas that could be improved are:
- `app/app.rb` - The main application file currently includes the routes or "controllers" for all the endpoints. These could be extracted further and stored in `app/controllers/` following a similar pattern to `app/models/`, leaving `app.rb` to only consist of application configuration and imports and thus follow the single responsibility principle.
- Currently, cassettes from the vcr gem store sensitive information (e.g open weather `APPID`), fortunately, vcr can be configure to filter out this information.

### More features
- Currently, the application only fetches current weather data but this could be extended to fetch weather forecasts as well.
- The Open Weather API supports various meta tags such as providing data using `metric` units. These can be used to provide more enriched weather information to the end user.
- The application uses a very minimal layout. This can be improved to provide better UI/UX.

### Fixing Capybara/Webmock interference
More research needs to be done on the topic of stubbing client-side requests which call external services. A starting point would be [here](http://www.thegreatcodeadventure.com/stubbing-api-calls-in-rspec-capybara-with-puffing-billy-and-vcr/).
Alternatively, javascript testing tools such as [mocha](https://mochajs.org) and [chai](http://chaijs.com) could be used for client tests.

### OS dependencies
In order to run tests with Capybara-webkit, you need to have `qmake` installed globally in your system, see [docs](https://github.com/thoughtbot/capybara-webkit/wiki/Installing-Qt-and-compiling-capybara-webkit). This is an intrusive requirement needed to only run a basic application. Other, less dependent testing frameworks could be considered or the application could be set up in a virtual environment.
