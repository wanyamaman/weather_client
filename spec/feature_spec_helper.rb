require 'capybara/rspec'
require 'capybara-webkit'
require File.expand_path '../../app/app.rb', __FILE__

Capybara.app = Sinatra::Application

RSpec.configure do |config|
  config.include Capybara::DSL, feature: true
  config.include Capybara::RSpecMatchers, feature: true
  Capybara.javascript_driver = :webkit
end

