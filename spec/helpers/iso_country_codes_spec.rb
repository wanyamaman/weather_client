require 'spec_helper'
require './app/sinatra/iso_country_codes'

RSpec.describe 'Sinatra Helpers' do
  class IsoHelper
    include Sinatra::ISOCountryCodes
  end

  describe 'ISOCountryCodes' do
    let(:codes) { IsoHelper.new }

    it 'returns an array of codes' do
      expect(codes.iso_3166_codes).to include(["United States of America", "US"])
    end
  end
end
