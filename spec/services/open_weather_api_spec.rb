require 'spec_helper'
require './app/services/open_weather_api'

RSpec.describe OpenWeatherApi do
  let(:api) { OpenWeatherApi.new }

  it 'defines a base uri' do
    expect(OpenWeatherApi.base_uri).to_not be(nil?)
  end

  context 'Name search' do
    describe '#get_by_city' do
      let(:paris_response) do
        VCR.use_cassette('open_weather/paris_valid_city') do
          api.get_by_city('paris')
        end
      end

      let(:x_response) do
        VCR.use_cassette('open_weather/x_invalid_city') do
          api.get_by_city('x')
        end
      end

      it 'returns 200 for valid city' do
        expect(paris_response['cod']).to eq(200)
      end

      it 'returns 404 for invalid city' do
        expect(x_response['cod'].to_i).to eq(404)
      end
    end

    describe '#get_by_city_and_country' do
      let(:valid_location_response) do
        VCR.use_cassette('open_weather/paris_valid_location') do
          api.get_by_city_and_country('paris', 'FR')
        end
      end

      let(:invalid_location_response) do
        VCR.use_cassette('open_weather/paris_invalid_location') do
          api.get_by_city_and_country('Paris', 'KE')
        end
      end

      let(:invalid_city_response) do
        VCR.use_cassette('open_weather/city_invalid_location') do
          api.get_by_city_and_country('xx', 'KE')
        end
      end

      it 'returns 200 for valid city, country combination' do
        expect(valid_location_response['cod']).to eq(200)
      end

      it 'returns 404 for invalid city, country combination' do
        expect(invalid_location_response['cod'].to_i).to eq(404)
        expect(api.api_response['sys']['country']).to eq('US')
      end

      it 'returns 404 for invalid city, valid country' do
        expect(invalid_city_response['cod'].to_i).to eq(404)
      end
    end
  end

  context 'Coordinates search' do
    describe '#get_by_coordinates' do
      let(:coords_named_response) do
        VCR.use_cassette('open_weather/coordinates_valid_named') do
          api.get_by_coordinates(35, 139)
        end
      end
      let(:coords_unnamed_response) do
        VCR.use_cassette('open_weather/coordinates_valid_unnamed') do
          api.get_by_coordinates(16.64, -179.3)
        end
      end
      let(:coords_invalid_response) do
        VCR.use_cassette('open_weather/coordinates_invalid') do
          api.get_by_coordinates(335, 139)
        end
      end

      it 'returns 200 for valid coordinates' do
        expect(coords_named_response['cod'].to_i).to eq(200)
        expect(coords_named_response['name']).to eq('Shuzenji')
      end

      it 'returns 200 for unmapped locations' do
        expect(coords_unnamed_response['cod'].to_i).to eq(200)
      end

      it 'returns 400 for invalid coordinates' do
        expect(coords_invalid_response['cod'].to_i).to eq(400)
      end
    end
  end
end
