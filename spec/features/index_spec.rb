require 'spec_helper'
require 'feature_spec_helper.rb'

RSpec.describe 'Home Page' do
  before(:all) do
    VCR.turn_off!
    WebMock.allow_net_connect!
  end

  after(:all) do
    VCR.turn_on!
    WebMock.disable_net_connect!(allow_localhost: true)
  end
  # Feature: Random Weather
  # As a viewer
  # I want to see the home page of the application
  # In order to view random weather information
  feature 'Random Weather' do
    # Scenario: viewer sees random weather info
    # Given I am a visitor
    # When I visit the home page
    # Then I see random weather
    scenario 'viewer sees random weather info', js: true do
      visit('/')

      expect(page).to_not have_css('.title', text: /Loading/, wait: 5)
      expect(page).to have_selector('#weather .latitude')
      expect(page).to have_selector('#weather .longitude')
      expect(page).to have_selector('#weather .description')
      expect(page).to have_selector('#weather .temp')
      expect(page).to have_selector('#weather .temp_max')
      expect(page).to have_selector('#weather .temp_min')
    end
  end

  # Feature: Weather Search
  # As a viewer
  # I want to see the home page of the application
  # In order to search for weather information
  feature 'Weather Search' do
    # Scenario: viewer sees location search form
    # Given I am a visitor
    # When I visit the home page
    # Then I see a form to search weather by country and city
    scenario 'viewer sees location search form' do
      visit('/')

      expect(page).to have_selector('form#location_search')
      expect(page).to have_selector('input#city')
      expect(page).to have_selector('select#country')
    end

    # Scenario: viewer can searches by valid location
    # Given I am a visitor
    # And I am on the home page
    # When I search by a valid location
    # Then I see weather information for that location
    scenario 'viewer searches by valid location', js: true do
      visit('/')
      fill_in 'city', with: 'paris'
      click_button 'Search'

      expect(page).to have_css('.title', text: 'Paris', wait: 10)
    end

    # Scenario: viewer searches by invalid location
    # Given I am a visitor
    # And I am on the home page
    # When I search by an invalid location
    # Then I see message with a warning
    scenario 'viewer searches by invalid location', js: true do
      visit('/')
      fill_in 'city', with: 'xx'
      click_button 'Search'
      message = 'We could not find that location. Please try again.'

      expect(page).to have_css('.title', text: 'No Location', wait: 10)
      expect(page).to have_css('#response_status', text: message, wait: 10)
    end
  end
end
