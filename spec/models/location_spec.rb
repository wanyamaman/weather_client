require 'spec_helper'
require './app/models/location'

RSpec.describe Location do
  let(:location) { Location.new }

  describe 'coordinates' do
    it 'generates coordinates when no city is provided' do
      expect(location.latitude).to_not be_nil
      expect(location.longitude).to_not be_nil
    end

    it 'does not generate coordinates when a city is provided' do
      @loc = Location.new('city' => 'nairobi')

      expect(@loc.latitude).to be_nil
    end

    it 'randomizes coordinates' do
      location.randomize!
      previous_lat = location.latitude
      previous_lon = location.longitude

      random = location.randomize!

      expect(location.latitude).to_not eq(previous_lat)
      expect(location.longitude).to_not eq(previous_lon)
      expect(random).to include(lat: location.latitude)
    end

    context 'invalid coordinates' do
      it 'should randomize incomplete coordinates' do
        @loc = Location.new('latitude' => 22)

        expect(@loc.latitude).to_not eq(22)
      end

      it 'should randomize non-integer coordinates' do
        @loc = Location.new('latitude' => 22, 'longitude' => 'abc')

        expect(@loc.latitude).to_not eq(22)
      end

      it 'should randomize out of range coordinates' do
        @loc = Location.new('latitude' => 22, 'longitude' => 1000)

        expect(@loc.latitude).to_not eq(22)
      end
    end
  end

  describe 'full location' do
    describe 'provides city and country' do
      let(:location) { Location.new('city' => 'paris', 'country' => 'US') }

      it 'returns true' do
        expect(location.full_location?).to be true
      end
    end

    describe 'missing country' do
      let(:location) { Location.new('city' => 'paris') }

      it 'returns false' do
        expect(location.full_location?).to be false
      end
    end

    describe 'missing city' do
      let(:location) { Location.new('country' => 'US') }

      it 'returns false' do
        expect(location.full_location?).to be false
      end
    end
  end

  describe 'sanitize values' do
    let(:location) { Location.new('city' => 'PAris', 'country' => '') }

    it 'should remove empty strings' do
      expect(location.country).to be nil
    end

    it 'should downcase city values' do
      expect(location.city).to eq('paris')
    end
  end
end
