require 'spec_helper'
require File.expand_path '../../../app/app.rb', __FILE__

RSpec.describe 'App' do
  def app
    Sinatra::Application
  end

  def make_internal_request(path)
    Net::HTTP.get_response('localhost', path, app.port)
  end

  context 'GET /' do
    it 'should be ok' do
      get '/'
      expect(last_response).to be_ok
    end
  end

  context 'GET /api/weather/random' do
    let(:random_response) do
      VCR.use_cassette('localhost/random') do
        make_internal_request('/api/weather/random')
      end
    end

    it 'should return status 200' do
      expect(random_response.code.to_i).to eq(200)
    end

    it 'should return a json response' do
      expect(random_response.content_type).to eq('application/json')
    end
  end

  context 'GET /api/weather/location' do
    describe 'valid location' do
      let(:city_valid_response) do
        VCR.use_cassette('localhost/nairobi_valid_city') do
          make_internal_request('/api/weather/location?city=nairobi')
        end
      end

      it 'should return 200' do
        expect(city_valid_response.code.to_i).to eq(200)
      end

      it 'should return a json response' do
        expect(city_valid_response.content_type).to eq('application/json')
      end

      it 'should have the correct name' do
        body = JSON.parse(city_valid_response.body)
        expect(body['name']).to eq('Nairobi')
      end
    end

    describe 'invalid location' do
      let(:city_invalid_response) do
        VCR.use_cassette('localhost/invalid_city') do
          make_internal_request('/api/weather/location?city=x')
        end
      end

      it 'should return not found' do
        expect(city_invalid_response.code.to_i).to eq(404)
      end
    end

    describe 'missing city' do
      let(:city_missing_response) do
        VCR.use_cassette('localhost/missing_city') do
          make_internal_request('/api/weather/location?country=US')
        end
      end

      it 'should return 400' do
        expect(city_missing_response.code.to_i).to eq(400)
      end
    end
  end
end
