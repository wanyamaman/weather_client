require 'httparty'

# Class for fetching data from the Open Weather Map Api.
# This implementation only fetches current weather.
# Weather can be fetched via: city name, country and city names or coordinates.
# API: https://openweathermap.org/current
# Parsed response from the open weather api is available via @api_response.
#
# E.g OpenWeatherApi.new.get_by_city("paris").api_response
# => {"cod": 200, "name": "paris" ...}
class OpenWeatherApi
  include HTTParty

  attr_reader :status, :api_response
  base_uri 'http://api.openweathermap.org/data/2.5/weather'

  def initialize
    @key = ENV['open_weather_key']
    @status = false
    @api_response = nil
  end

  # Returns weather information based on a location using json format.
  # Valid locations consist of either a city name alone or
  # both city and conutry names. Country names use ISO 3166-1 aplha-2 format.
  #
  # E.g
  # get_by_city("paris") => {"cod": 200, "name": paris, ...}
  # get_by_city("", "UK") => {"cod": 404, "message": 'city not found'}
  def get_by_city(city, country = nil)
    search_terms = [city, country].compact.join(',')
    query = {q: search_terms}

    @api_response = http_get(query).parsed_response

    if @api_response['name']&.downcase != city
      @status = '404'
      return {'cod' => '404', 'message' => 'city not found'}
    else
      @status = @api_response['cod']
    end

    @api_response
  end

  # Wrapper function for get_by_city() method.
  # Fetches weather by both country and city names.
  # Verifies the response country matches the query country, this prevents
  # best match responses.
  #
  # E.g
  # get_by_city_and_country("paris", "UK") => {"cod": 404, "name": paris, ...}
  def get_by_city_and_country(city, country)
    response = get_by_city(city, country)

    if response.fetch('sys', {})['country'] != country
      @status = '404'
      return {'cod' => '404', 'message' => 'city not found'}
    end
    response
  end

  # Returns weather information based on latitude and longitude coordinates.
  # Returns response in json format.
  #
  # E.g
  # get_by_coordinates(335, 139)
  #   => {"cod": 200, "name": "Shuzenji", ...}
  def get_by_coordinates(latitude, longitude)
    query = {lat: latitude, lon: longitude}

    @api_response = http_get(query).parsed_response
    @status = @api_response['cod']

    @api_response
  end

  private

  # Makes actual calls to the Weather Map Api using HTTPary gem.
  # Accepts a query dictionary.
  def http_get(query = {})
    query['APPID'] = @key
    HTTParty.get(self.class.base_uri, query: query)
  end
end
