# Class for creating location objects.
# Valid locations can only be represented by either:
# 1. a city name
# 2. both city and country names
# 3. both latitude and longitude coordinates
#
# Location.new.latitude
#   => 23.8984
class Location
  attr_reader :city, :country, :latitude, :longitude

  # Verifies input using helper methods before creating an instance.
  #
  # E.g
  # Location.new({country: "US"}).latitude    --> invalid input
  #   => 55.3   --> "A random coordinate anywhere in the world"
  def initialize(options = {})
    cleaned_options = sanitize(options)

    @city = cleaned_options['city']
    @country = cleaned_options['country']
    @latitude = cleaned_options['latitude']
    @longitude = cleaned_options['longitude']

    randomize! unless valid?
  end

  # Clears all location fields and sets random coordinates.
  # Returns a hash of coordinates.
  #
  # Eg.
  # input = {latitude: 10, longitude: 20}
  # Location.new(input).randomize!
  #  => {lat: 55, lon: 23}
  def randomize!
    @city = nil
    @country = nil
    @latitude = rand(-90.0..90.0)
    @longitude = rand(-180.0..180.0)
    { lat: @latitude, lon: @longitude }
  end

  # Returns true if both city and country fields are populated.
  #
  # E.g
  #  place = Location.new({city: "paris", country: "FR"})
  #  place.full_location?
  #    => true
  def full_location?
    return true if @city && @country
    false
  end

  private

  # Used to verify input during object initialization.
  # A location is valid if it has either a city name or a pair of coordinates.
  def valid?
    return false unless @city || @latitude && @longitude

    return false unless valid_or_nil_coords?

    true
  end

  # Used to verify input during object initialization.
  # Returns true only if both longitude and latitude coordinates are valid
  # or nil. Otherwise it returns false.
  def valid_or_nil_coords?
    if @latitude && @longitude
      lat = Integer(@latitude)
      lon = Integer(@longitude)

      return false unless lat.between?(-90, 90) && lon.between?(-180, 180)
    elsif @latitude || @longitude
      return false
    end
    true
  rescue ArgumentError
    false
  end

  # Used to cleanse input during initialization.
  # Removes nil or empty string values. Returns cleansed hash.
  #
  # Eg.
  # sanitize({city: nil, country: ''})
  #   => {}
  def sanitize(options)
    options.delete_if { |_k, v| [nil, ''].include?(v) }
    options['city']&.downcase!
    options
  end
end
