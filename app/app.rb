require 'sinatra'
require_relative 'models/init'
require_relative 'sinatra/iso_country_codes'
require_relative 'services/init'

set :app_file, __FILE__

# Root of application.
# Consists of routes, application imports and configuration.
get '/' do
  erb :"index/show"
end

get '/api/weather/random', provides: :json do
  content_type :json

  @location = Location.new
  api = OpenWeatherApi.new
  response = api.get_by_coordinates(@location.latitude, @location.longitude)

  status response['cod'].to_i
  response.to_json
end

get '/api/weather/location', provides: :json do
  content_type :json

  location = Location.new(params)
  api = OpenWeatherApi.new

  if location.full_location?
    response = api.get_by_city_and_country(location.city, location.country)
  else
    response = api.get_by_city(location.city)
  end

  status response['cod'].to_i

  response.to_json
end
