$(document).ready(function() {

  $("select[name='country']").focus(function() {
    $("#country_default").removeAttr("disabled");
  });

  // Fetch random weather
  $.ajax({
    url: "/api/weather/random",
    type: "GET",
    dataType: "json",
    contentType: "application/json; charset=utf8",
    success: function(data) {
      var title = data.name || "Random Location";

      if (_userHasNotSearched()) {
        _renderTable(data, "success", title);
      }
    },
    error: function(xhr, textStatus, error) {
      _renderTable({}, "error", "");

      $("#response_status").html(
        "A random station could not be shown this time"
      );
    }
  });

  // Fetch weather by location
  $("#location_search").submit(function(event) {
    event.preventDefault();
    if($("#country").val() == ""){
      $("#country_default").attr("disabled","disabled");
    }

    $.ajax({
      url: "/api/weather/location",
      type: "GET",
      data: $(this).serialize(),
      dataType: "json",
      contentType: "application/json; charset=utf8",
      success: function(data) {
        var title = "";
        if (data.name) {
          title = "Result - " + data.name;
        } else {
          title = "Result";
        }

        _renderTable(data, "success", title);
      },
      error: function(xhr, textStatus, error) {
        _renderTable({}, "error", "");

        $("#response_status").html(
          "We could not find that location. Please try again."
        );
      }
    });
  });

  // Private functions
  function _renderTable(data, status, title) {
    if (status == "error") {
      $("#weather_table td").html("-");
      $("#weather .title").text("No Location");
    } else {
      $("#response_status").text("...");
      $("#weather .title").text(title);
      $("#weather .description").text(data.weather[0].description);
      $("#weather .temp").text(data.main.temp);
      $("#weather .temp_max").text(data.main.temp_max);
      $("#weather .temp_min").text(data.main.temp_min);
      $("#weather .latitude").text(data.coord.lat);
      $("#weather .longitude").text(data.coord.lon);
    }
  }

  function _userHasNotSearched() {
    var text = $(".title").text()
    if (text.indexOf("Result") !== -1) {
      return false;
    } else {
      return true;
    }
  }

});
